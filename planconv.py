import pandas as pd
import json

df = pd.read_excel('vortragsplan.xlsx', sheet_name='Tabelle1', header=1)

schedule = []
times = {}

for idx, ser in df.iterrows():

    if pd.notna(ser['Zeit']):
        times[ser['Zeit']] = idx

for room, ser in df.items():
    if not room.startswith('Unnamed') and not room.startswith('Zeit'):
        roomo = {'name': room}
        roomtimes = []
        for t, i in times.items():
            if pd.notna(ser[i]):
                tl = {'time': t}
                lecture = {}
                lecture['fs'] = ser[i]
                lecture['name'] = ser[i + 1]
                if pd.isna(lecture['name']):
                    lecture['name']= "" 
                lecture['teaser'] = ser[i + 2]
                if pd.isna(lecture['teaser']):
                    lecture['teaser'] = ser[i]
                lecture['time'] = t
                lecture['room'] = room
                tl['lecture'] = lecture
                roomtimes.append(tl)

        roomo['times'] = roomtimes
        schedule.append(roomo)

print(times)
print(schedule)

jsondata = {'schedule': schedule, 'version': '20190515001'}
with open('lectures.json', 'w') as outfile:
    json.dump(jsondata, outfile, indent=1)
